#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include "fileio.h"

int getFileLength(FILE* fp){
	int size = 0;
	fseek(fp, 0, SEEK_END);
	size = ftell(fp);
	fseek(fp, 0, SEEK_SET);
	//printf("%d\n",size);
	return size;
}

FILE* openFile(const char* fileName, const char* fileMode){
	FILE* filePtr;
	int iErr = EXIT_SUCCESS;
	
	if((filePtr = fopen(fileName, fileMode)) != NULL)
	{
		printf("%s was successfully opened.\n", fileName);
//		FILE* retFile = (FILE*)malloc(getFileLength(filePtr));
//		retFile = filePtr;
		return filePtr;
	}
	else{
		iErr = errno;
		printf("Error accessing the file %s: %s\n", fileName, strerror(iErr));
		return NULL;
	}	
}
int readFile(FILE* filePtr, BYTE** data, int* fileSize){
	int iErr = 0;

	*fileSize = getFileLength(filePtr);
	BYTE* file = (BYTE*)malloc(*fileSize);
	if(!sizeof(file)){
		iErr = ferror(filePtr);
		return iErr;
	}
	fread((void*) file, *fileSize,1, filePtr);
	if(!file){
		iErr = ferror(filePtr);
	//	free(file);
	//	file = NULL;
		return iErr;
	}
	else{
		*data = file;
	}
//	free(file);
//	file = NULL;
	return iErr;
}

int writeFile(FILE* filePtr, BYTE* data, int fileSize){
	
	int size = getFileLength(filePtr);
	int iErr = 0;
	int writeErr = fwrite((void*)data,sizeof(BYTE),fileSize, filePtr);
	if(writeErr == 0 ){
		if((iErr = ferror(filePtr))){
			printf("%d\n", iErr);
			printf("Error writing to the file\n");
		}
	}
	return iErr;
}
