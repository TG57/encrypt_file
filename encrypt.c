#include <stdio.h>
#include <stdlib.h>
#include "encrypt.h"

void scrambleData(BYTE* data, int size, BYTE_FUNC bf){
	for(int i=0; i<size; i++){
		data[i] = bf(data[i]);
	}
}
BYTE swap_nibbles(BYTE byte){
	return byte << 4 | byte >> 4;
}

BYTE mix_bits(BYTE byte){
	BYTE edge = byte<<6 | byte >>6;
	int left_mid_mask = 48;
	BYTE left_mid = byte & left_mid_mask;
       	BYTE right_mid_mask = 12;
	BYTE right_mid = byte & right_mid_mask;
	BYTE middle = right_mid << 2 | left_mid >> 2;
	return edge | middle;
}
