# encrypt_file
This reads a file, encrypts the data, then writes to another file.
## Name
Encrypt File
## Description
This program will take in a file name and encrypt each byte with a certain algorith that alters the bits by shifting and using bitwise operators. You will have to input the file names in the program.c file in the main function and the type of encryption from options of either mix_bits or swap_nibbles. When you want to decrypt it you can run the same type of encryption of the file that was encrpyted.
## Usage
This is used to encrypt and decrypt files
## Contributing
I am open to anyone wanting to improve this project.
## Project status
This was just a little practice project but I'd like to make it so you can input from the command line and more encrpytion types.
