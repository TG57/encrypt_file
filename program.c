#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "fileio.h"
#include "encrypt.h"

//Purpose: Test the file io and make sure it reads and writes properly
//Parameters:
//	
//Returns: void
void testFileIO(){
	FILE* fileRead = openFile("datafile.docx", "rb");
	FILE* fileWrite = openFile("datafilescramble.docx", "wb");
	BYTE* data = NULL;
	int fileSize =0;
	int iErrRead = readFile(fileRead, &data, &fileSize);
//	printf("%c\n",*data);
	int iErrWrite = writeFile(fileWrite, data, fileSize);
	fclose(fileRead);
	fclose(fileWrite);
}

//Purpose: Test the encrypt and make sure it works. reads the file to the 
//data variable and then encrypt the data variable with the bf function and 
//writes the new data to the write file. This also works for decrypting it, 
//you just have to make the file that was encrypted to the read file and a 
//new file to the write file.
//Parameters:
//	read - a string of the read file name
//	write - a string of the write file name
//	bf - a BYTE_FUNC that encrypts the bytes
//Returns: void
void testScramble(char* read, char* write, BYTE_FUNC bf){	
	FILE* fileRead = openFile(read, "rb");
	FILE* fileWrite = openFile(write, "wb");
	BYTE* data = NULL;
	int fileSize =0;
	int iErrRead = readFile(fileRead, &data, &fileSize);	
	fclose(fileRead);
	//Scramble the data
	scrambleData(data, fileSize, bf);
	
	int iErrWrite = writeFile(fileWrite, data, fileSize);
	free(data);
	data = NULL;
	fclose(fileWrite);
}
int main(){
//	testFileIO();		
	testScramble("datafile.docx", "datascramble.docx", mix_bits);
	return 0;
}
