#ifndef FILEIO_H
#define FILEIO_H

typedef unsigned char BYTE;

//Purpose: Calculate the length of a file passed in
//Parameters:
//	fp - a FILE pointer pointing to an open file
//Returns: the length of the file in bytes
int getFileLength(FILE* fp);

//Purpose: open a file
//Parameters:
//	fileName - the name and extensions of the file passed in
//	fileMode - the mode of the file "rb" or "wb" for example
//Returns: A FILE pointer to open file
FILE* openFile(const char* fileName, const char* fileMode);

//Purpose: Read some data from a file.
//Parameters:
//	filePtr - FILE pointer to open file
//	data - a pointer to a pointer to the byte data
//	filesize - a pointer to the size of the file in bytes
//Returns: An error number returned from the ferror funstion
int readFile(FILE* filePtr, BYTE** data, int* fileSize);

//Purpose: Write some data out to file
//Parameters:
//	filePtr - FILE pointer to an open file
//	data - a pointer to the byte data
//	filesize - size of the file in bytes
//Returns: An error number returned from the ferror function
int writeFile(FILE* filePtr, BYTE* data, int fileSize);

#endif
