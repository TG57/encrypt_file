#ifndef ENCRYPT_H
#define ENiCRYPT_H
#include "fileio.h"
//Purpose: this is a typedef of a function that takes in a BYTE and returns
//a BYTE
//Parameters:
//	byte - a BYTE of the file
//Returns: the byte after it has been altered
typedef BYTE(*BYTE_FUNC)(BYTE byte);

//Purpose: Calculate the length of a file passed in
//Parameters:
//	data - a BYTE array of the bytes in a file
//	size - the number of bytes in the file
//	bf - a BYTE_FUNC that will encrypt the byte
//Returns: void
void scrambleData(BYTE* data, int size, BYTE_FUNC bf);

//Purpose: Swap the nibbles(a nibble is half of the byte or 4 bits) so it
//	will shift the byte 4 places left and OR that with the byte shifted
//	4 spaces right.
//Parameters:
//	byte - The Byte of the file
//Returns: The byte after its been altered
BYTE swap_nibbles(BYTE byte);

//Purpose: swap the positions of nibbles then the pairs of bits in the nibbles for example 10(01)11(10) will turn into (10)11(01)10.
//Parameters:
//	byte - a byte from the file
//Returns: The byte after its been altered
BYTE mix_bits(BYTE byte);
#endif
